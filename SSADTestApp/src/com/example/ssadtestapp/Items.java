package com.example.ssadtestapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.VideoView;

public class Items extends Activity implements View.OnClickListener{
	int flag=0;
	VideoView Video;
	ImageView Image;
	RadioButton rB1, rB2, rB3, rB4;
	Intent i;
	Uri videoUri;
	final static int videoData = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.items);
		Video = (VideoView)findViewById(R.id.videoView);
		rB1 = (RadioButton)findViewById(R.id.rB1);
		rB2 = (RadioButton)findViewById(R.id.rB2);
		rB3 = (RadioButton)findViewById(R.id.rB3);
		rB4 = (RadioButton)findViewById(R.id.rB4);
		Image = (ImageView)findViewById(R.id.Image); 
		rB1.setOnClickListener(this);
		rB2.setOnClickListener(this);
		rB3.setOnClickListener(this);
		rB4.setOnClickListener(this);
	}
	
	@Override 
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.Share){
        	Intent shareIntent = new Intent();
        	shareIntent.setAction(Intent.ACTION_SEND);
        	shareIntent.putExtra(Intent.EXTRA_STREAM, videoUri);
        	shareIntent.setType("video/mp4");
        	startActivity(Intent.createChooser(shareIntent, "Send Birthday Wishes... "));
        }
        return super.onOptionsItemSelected(item);
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.rB1:
			i = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
			flag = 1;
			startActivityForResult(i, videoData);
			break;
		case R.id.rB2:
			i = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
			flag = 2;
			startActivityForResult(i, videoData);
			break;
		case R.id.rB3:
			flag = 3;
			i = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
			startActivityForResult(i, videoData);
			break;
		case R.id.rB4:
			flag = 4;
			i = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
			startActivityForResult(i, videoData);
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if(arg1 == RESULT_OK){
			videoUri = arg2.getData();
			Video.setVideoURI(videoUri);
			if(flag == 2){
				Image.setImageResource(R.drawable.choco);
				flag =0 ;
			}
			else if(flag == 1){
				flag =0 ;
				Image.setImageResource(R.drawable.balloons);
			}
			else if(flag == 3){
				flag =0 ;
				Image.setImageResource(R.drawable.cake);
			}
			else if(flag == 4){
				flag =0 ;
				Image.setImageResource(R.drawable.gift);
			}
			Video.start();
		}
	}
}
